package presentation;

public class UserAccessException extends Exception{

    public UserAccessException(String mensaje, Throwable err) {
        super(mensaje, err);
    }

    public UserAccessException(String mensaje) {
        super(mensaje);
    }

}
