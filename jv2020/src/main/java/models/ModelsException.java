package models;

public class ModelsException extends Exception{

    public ModelsException(String mensaje, Throwable err) {
        super(mensaje, err);
    }

    public ModelsException(String mensaje) {
        super(mensaje);
    }

}
