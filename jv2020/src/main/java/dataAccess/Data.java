package dataAccess;

import models.Mail;
import models.Nif;
import models.Password;
import models.Session;
import models.Simulation;
import models.User;
import models.User.RoleUser;
import utils.EasyDate;

import java.util.ArrayList;
import java.util.HashMap;

public class Data {

	public final static int MAX_DATA = 10;

	private ArrayList<User> usersData;
	private ArrayList<Session> sessionsData;
	private ArrayList<Simulation> simulationsData;
	private HashMap<String, String> userIdMail = new HashMap<String, String>();
	private int registeredUser;
	private int registeredSessions;
	private int registeredSimulations;

	public Data() {		
		this.usersData = new ArrayList<>();
		this.sessionsData = new ArrayList<>();
		this.simulationsData = new ArrayList<>();
		this.registeredUser = 0;
		this.registeredSessions = 0;
		this.registeredSessions = 0;
		loadIntegratedUsers();
	}

	private void loadIntegratedUsers() {
		this.createUser(new User(new Nif("00000000T"),
				"Admin",
				"Admin Admin",
				"La Iglesia, 0, 30012, Patiño",
				new Mail("admin@gmail.com"),
				new EasyDate(2000, 1, 14),
				new EasyDate(2021, 1, 14),
				new Password("Miau#00"), 
				RoleUser.REGISTERED
				));
		this.createUser(new User(new Nif("00000001R"),
				"Guest",
				"Guest Guest",
				"La Iglesia, 0, 30012, Patiño",
				new Mail("guest@gmail.com"),
				new EasyDate(2000, 1, 14),
				new EasyDate(2021, 1, 14),
				new Password("Miau#00"), 
				RoleUser.REGISTERED
				));
	}

	//Búsqueda binaria. Si el usuario está contenido en la lista devuelve el índice.
	//Si el usuario no está devuelve (-(punto de inserción)-1)
	//El punto de inserción es el índice dónde el usuario se debería insertar en la lista.
	//Hay que restarle uno porque si no no sería posible distinguir entre un usuario que está en el índice 0
	//y un usuario que no está y se debe insertar en el índice 0.
	private int busquedaBinariaUsuario(String id) {
		int l = 0;
		int r = usersData.size() - 1;
		int m = 0;
		while (l <= r) {
			m = (l + r) / 2;
			int c = usersData.get(m).getId().compareTo(id);
			if (c < 0) {
				l = m + 1;
			} else if (c > 0) {
				r = m - 1;
			} else {
				return m;
			}
		}
		return -m - 1;
	}

	private int busquedaBinariaSesion(String id) {
		int l = 0;
		int r = sessionsData.size() - 1;
		int m = 0;
		while (l <= r) {
			m = (l + r) / 2;
			int c = sessionsData.get(m).getId().compareTo(id);
			if (c < 0) {
				l = m + 1;
			} else if (c > 0) {
				r = m - 1;
			} else {
				return m;
			}
		}
		return -m - 1;
	}

	private int busquedaBinariaSimulacion(String id) {
		int l = 0;
		int r = simulationsData.size() - 1;
		int m = 0;
		while (l <= r) {
			m = (l + r) / 2;
			int c = simulationsData.get(m).getId().compareTo(id);
			if (c < 0) {
				l = m + 1;
			} else if (c > 0) {
				r = m - 1;
			} else {
				return m;
			}
		}
		return -m - 1;
	}

	// Users

	public User findUser(String id) {
		if (id == null) return null;
		int i = busquedaBinariaUsuario(id);
		if (i < 0) return null;
		return usersData.get(i);
	}

	public void createUser(User user) {
		int i = busquedaBinariaUsuario(user.getId());
		if (i < 0) {
			usersData.add(-(i+1), user);
			userIdMail.put(user.getId(), user.getMail().getTexto());
		}
	}

	public void updateUser(User user) {
		int i = busquedaBinariaUsuario(user.getId());
		if (i >= 0) {
			usersData.set(i, user);
			userIdMail.put(user.getId(), user.getMail().getTexto());
		}
	}

	public void deleteUser(String id) {
		int i = busquedaBinariaUsuario(id);
		if (i >= 0) {
			usersData.remove(i);
			userIdMail.remove(id);
		}
	}

	private int indexOfUser(User user) {
		return busquedaBinariaUsuario(user.getId());
	}

	// Sessions

	public Session findSession(String id) {
		if (id == null) return null;
		int i = busquedaBinariaUsuario(id);
		if (i < 0) return null;
		return sessionsData.get(i);
	}

	public void createSession(Session session) {
		int i = busquedaBinariaUsuario(session.getId());
		if (i < 0) {
			sessionsData.add(-(i+1), session);
		}
	}

	public void updateSession(Session session) {
		int i = busquedaBinariaUsuario(session.getId());
		if (i >= 0) {
			sessionsData.set(i, session);
		}
	}

	private int indexOfSession(Session session) {
		return busquedaBinariaUsuario(session.getId());
	}
	
	// Simulations

	public Simulation findSimulation(String id) {
		if (id == null) return null;
		int i = busquedaBinariaUsuario(id);
		if (i < 0) return null;
		return simulationsData.get(i);
	}

	public void createSimulation(Simulation simulation) {
		int i = busquedaBinariaUsuario(simulation.getId());
		if (i < 0) {
			simulationsData.add(-(i+1), simulation);
		}
	}
	
	public void updateSimulation(Simulation simulation) {
		int i = busquedaBinariaUsuario(simulation.getId());
		if (i >= 0) {
			simulationsData.set(i, simulation);
		}
	}

	private int indexOfSimulation(Simulation simulation) {
		return busquedaBinariaUsuario(simulation.getId());
	}

}
