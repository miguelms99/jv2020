package dataAccess;

public class DataException extends Exception{

    public DataException(String mensaje, Throwable err) {
        super(mensaje, err);
    }

    public DataException(String mensaje) {
        super(mensaje);
    }

}
